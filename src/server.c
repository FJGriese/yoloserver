/*
 * server.c
 *
 *  Created on: Oct 23, 2018
 *      Author: fritz
 */

#include <stdlib.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <memory.h>
#include <stdint.h>
#include <errno.h>

#include <image.h>

#define PORT 11000
#define WRITE_BUFFER_SIZE 4096

int listenfd = -1;
int clientfd = -1;

char client_write_buffer[WRITE_BUFFER_SIZE];

int connected_to_client = 0;

struct sockaddr_in serv_addr;

unsigned char *matrix_buffer;

int send_int(int num) {
	int fd = clientfd;
	int32_t conv = htonl(num);
	char *data = (char*) &conv;
	int left = sizeof(conv);
	int rc;
	do {
		rc = write(fd, data, left);
		if (rc < 0) {
			if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
				// use select() or epoll() to wait for the socket to be writable again
			} else if (errno != EINTR) {
				return -1;
			}
		} else {
			data += rc;
			left -= rc;
		}
	} while (left > 0);
	return 0;
}

int sendDetectionsHeader(char *matrix, int num, int total_size) {
	//total size of header should be 128 + 4 + 4 = 136 bytes
	printf("Sending detections header...\n");
	if (write(clientfd, matrix_buffer, 128) == -1)
		printf("ERROR: was not able to send detections header.\n");
	else {
		printf("Done!.\n");
	}
	free(matrix_buffer);
	send_int(num);
	send_int(total_size);
	return 0;
}

int sendDetectionBox(int left, int top, int right, int bottom, float red,
		float green, float blue, char *name) {
	send_int(left);
	send_int(top);
	send_int(right);
	send_int(bottom);

	send_int(red * 255);
	send_int(green * 255);
	send_int(blue * 255);

	int size = strlen(name);
	send_int(size);
	write(clientfd, name, size);
	return 0;
}

int send_detections(image im, char * matrix, detection *dets, int num,
		float thresh, char **names, image **alphabet, int classes) {
	// TODO clean up.
	int i, j;
	int total_size = 0;
	int detected = 0;

	for (i = 0; i < num; ++i) {
		int classes_found = -1;
		for (j = 0; j < classes; ++j) {
			if (dets[i].prob[j] > thresh) {
				if (classes_found < 0) {
					detected++;
					total_size += strlen(names[j]);
					total_size += 32; //8 * 4 Bytes ints for box parameters.
					classes_found = j;
					printf("Detection found. header size: %d.\n",
							total_size);
				} else {
					// TODO when a detection has several classes,
					// add only additional string length to total_size.
				}
			}
		}
	}
	sendDetectionsHeader(matrix, detected, total_size);

	for (i = 0; i < num; ++i) {
		char labelstr[4096] = { 0 };
		int classes_found = -1;
		for (j = 0; j < classes; ++j) {
			if (dets[i].prob[j] > thresh) {
				if (classes_found < 0) {
					classes_found = j;
				}
				printf("Sending detection box for %s.\n", names[j]);
				strcat(labelstr, names[j]);
				break;
			}
		}
		if (classes_found >= 0) {

			int offset = classes_found * 123457 % classes;
			float red = get_color(2, offset, classes);
			float green = get_color(1, offset, classes);
			float blue = get_color(0, offset, classes);
			float rgb[3];

			//width = prob*20+2;

			rgb[0] = red;
			rgb[1] = green;
			rgb[2] = blue;
			box b = dets[i].bbox;

			int left = (b.x - b.w / 2.) * im.w;
			int right = (b.x + b.w / 2.) * im.w;
			int top = (b.y - b.h / 2.) * im.h;
			int bot = (b.y + b.h / 2.) * im.h;

			if (left < 0)
				left = 0;
			if (right > im.w - 1)
				right = im.w - 1;
			if (top < 0)
				top = 0;
			if (bot > im.h - 1)
				bot = im.h - 1;

			sendDetectionBox(left, top, right, bot, red, green, blue, labelstr);
		}
	}
}

unsigned char * recieveMatrixString() {
	// TODO clean up.
	printf("recieving matrix\n");
	ssize_t byte_count;
	int32_t matrix_size = 128; //2* 16 * 4byte floats for 4x4 matrix
	matrix_buffer = malloc(matrix_size);
//	printf("\nmatrix before:\n");
//	for(int i=0; i<128; i = i +4) {
//		printf("%f\n", (float)(*(matrix_buffer+i)));
//	}
	unsigned char *buffer_marker = matrix_buffer;
	ssize_t total_matrix_bytes_recieved = 0;
	while (total_matrix_bytes_recieved < matrix_size) {
		byte_count = recv(clientfd, buffer_marker,
				matrix_size - total_matrix_bytes_recieved, 0);
		if (byte_count <= 0) {
			printf("Something went wrong receiving matrix data\n");
		}
		buffer_marker += byte_count;
		total_matrix_bytes_recieved += byte_count;
	}
	if (total_matrix_bytes_recieved > matrix_size) {
		printf("somehow we got too many bytes\n");
	}
	printf("recieved %d bytes\n", total_matrix_bytes_recieved);
//	printf("matrix adresse: %i, %c\n", &(*matrix_buffer), (float)(*(matrix_buffer)));
//	printf("\nmatrix after:\n");
//	for(int i=0; i<128; i = i +4) {
//		printf("%f\n", (float)(*(matrix_buffer+i)));
//	}

	//printf("matrix adresse: %i, %c\n", &(*matrix_buffer), (float)(*(matrix_buffer)));
	return matrix_buffer;
}

write_jpeg(unsigned char * buffer, int bufferSize, char * filename) {
	FILE * outfile;
	if ((outfile = fopen(filename, "wb")) == NULL) {
		fprintf(stderr, "can't open %s\n", filename);
		exit(1);
	}

	fwrite(buffer, 1, bufferSize, outfile);
	fclose(outfile);
}

void recieve_frame() {
	printf("recieving");
	ssize_t byte_count;
	int32_t frame_size = 0;
	byte_count = recv(clientfd, &frame_size, sizeof(int32_t), 0);
	if (byte_count <= 0) {
		printf("Something went wrong recieving frame size\n");
	}
	frame_size = ntohl(frame_size);
	printf(" frame size: %d\n", frame_size);
	unsigned char *frame_buffer = malloc(frame_size);
	unsigned char *buffer_marker = frame_buffer;
	ssize_t total_frame_bytes_recieved = 0;
	while (total_frame_bytes_recieved < frame_size) {
		byte_count = recv(clientfd, buffer_marker,
				frame_size - total_frame_bytes_recieved, 0);
		if (byte_count <= 0) {
			printf("Something went wrong recieving frame data\n");
		}
		buffer_marker += byte_count;
		total_frame_bytes_recieved += byte_count;
	}
	if (total_frame_bytes_recieved > frame_size) {
		printf("somehow we got too many bytes\n");
	}
	printf("recieved %d bytes\n", total_frame_bytes_recieved);
	write_jpeg(frame_buffer, frame_size, "next_image.jpg");
	//CvMat mat = cvMat(1152,2048,CV_8UC3, (void*) frame_buffer);

	//IplImage* img = cvDecodeImage(&mat, CV_LOAD_IMAGE_COLOR);

	free(frame_buffer);
	//return img;
}

int accept_client_connection() {

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(PORT);

	bind(listenfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr));

	listen(listenfd, 10);
	printf("Waiting for client...\n");
	clientfd = accept(listenfd, (struct sockaddr*) NULL, NULL);

	shutdown(listenfd, SHUT_RDWR);
	close(listenfd);

	printf("client connected\n");

	connected_to_client = 1;

	return 0;
}

int terminate_client_connection() {
	shutdown(clientfd, SHUT_RDWR);
	connected_to_client = 0;
	return close(clientfd);
}

get_image_from_client() {
	if (connected_to_client != 1) {
		accept_client_connection();
	}
	recieve_frame();
}

//int main(int argc, char **argv) {
//	accept_client_connection();
//
//
//		recieve_frame();
//
//		int  stdout_bk; // is fd for stdout backup
//
//		   stdout_bk = dup(fileno(stdout));
//
//		   int pipefd[2];
//		   pipe2(pipefd, 0); // O_NONBLOCK);
//
//		   // What used to be stdout will now go to the pipe.
//		   dup2(pipefd[1], fileno(stdout));
//
//		   system("/home/yolo/git/darknet/darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights test.jpg");
//
//		   fflush(stdout);//flushall();
//		   write(pipefd[1], "", 1); // null-terminated string!
//		   close(pipefd[1]);
//
//		   dup2(stdout_bk, fileno(stdout));//restore
//
//		   char buf[1001];
//		   read(pipefd[0], buf, 1000);
//		   printf("got this from the pipe >>>%s<<<\n", buf);
//
//		   char* width_index = strstr(buf, "width: ") + 7;
//
//		   printf("found %s\n", width_index);
//
//		   char * delimiter_index = strstr(width_index, ",");
//		   int width_size = delimiter_index - width_index;
//
//		   char width[width_size + 1];
//		   memcpy(width, width_index, width_size);
//		   width[width_size] = '\0';
//		   printf("%s extracted.\n", width);
//
//
//	terminate_client_connection();
//}

